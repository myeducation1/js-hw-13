function create (){
  return document.createElement("div");
}
function addList(elem, list){
  return elem.classList.add(list);
}

let allCards = create();
addList(allCards, "all-cards");


let link = "https://swapi.dev/api/people/";

let firstFetch = fetch(link);
let twoFetch = firstFetch.then(rez => rez.json());
twoFetch.then(function (rez){
  let [...rezultat] = rez.results;
  console.log(rez)
  rez.results.forEach((elem) => {
    let cards = create();
    addList(cards, "cards");
    let names = create();
    addList(names, "name");
    let yaar = create();
    addList(yaar, "yaar");
    let mela = create();
    addList(mela, "mela");
    cards.append(names);
    cards.append(yaar);
    cards.append(mela);
    allCards.append(cards);
    names.innerText = elem.name;
    yaar.innerText = elem.birth_year;
    mela.innerText = elem.gender;
  })
})

document.body.append(allCards);